import React, {Component} from 'react'
import { CanvasWidget } from '@projectstorm/react-canvas-core';
import './index.css';


class ReactDiagram extends Component{
  constructor(props){
    super(props)
  }
  render(){
    const engine = this.props.engine
    return(<div><CanvasWidget  className="diagram-container" engine={engine} /></div>)
  }
}

export default ReactDiagram