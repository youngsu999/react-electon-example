import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
// import App from './App'
import ReactDiagram from './reactDiagram'
// import Stopwatch from './components/Stopwatch'
import * as serviceWorker from './serviceWorker'
import createEngine, { DiagramModel, DefaultNodeModel, DefaultLinkModel } from '@projectstorm/react-diagrams';
// import * as beautify from 'json-beautify';


//1) setup the diagram engine
let engine = createEngine();
//2) setup the diagram model
var model = new DiagramModel();

function init_model(model, engine){

    //3-A) create a default node
    let node1 = new DefaultNodeModel({
        name: 'Node 1',
        color: 'rgb(0,192,255)'
    });
    node1.setPosition(100, 100);
    let port1 = node1.addOutPort('Out');
    
    //3-B) create another default node
    let node2 = new DefaultNodeModel('Node 2', 'rgb(192,255,0)');
    let port2 = node2.addInPort('In');
    node2.setPosition(400, 100);
    
    // link the ports
    // let link1 = port1.link<DefaultLinkModel>(port2); 
    //type script 를 추가로 써야 하나?
    let link1 = port1.link(port2);
    link1.getOptions().testName = 'Test';
    link1.addLabel('Hello World!');
    
    // 4) add the models to the root graph
    model.addAll(node1, node2, link1);
    engine.setModel(model); //-> render 되고 canvas 의 정보를 읽어올 수 있음.
}

init_model(model, engine)


const onClick = ()=>{
    //3-B) create another default node
    let node = new DefaultNodeModel('Node 2', 'rgb(192,255,0)');
    let port = node.addInPort('In');
    node.setPosition(300, 100);
    model.addAll(node)
    engine.setModel(model); //-> render 되고 canvas 의 정보를 읽어올 수 있음.
}

const onClick2 = ()=>{
    // let model_s = model.serialize()
    let str = JSON.stringify(model.serialize());
    let model_ = new DiagramModel();
    // model_.deserializeModel(model_s, engine)
    model_.deserializeModel(JSON.parse(str), engine);
    engine.setModel(model_)
    // garbage collection 이 issue가 될 수 있다.
    model = null
    model = model_
}

ReactDOM.render(
    (<div>
        <ReactDiagram  engine={engine}  />
        <button onClick={onClick}>
            Activate Lasers
        </button>
        <button onClick={onClick2}>
            Activate Lasers
        </button>
    </div>), 
    document.getElementById('root')
)

// ReactDOM.render(<App />, document.getElementById('main2'))
// ReactDOM.render(<Stopwatch />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
