import React ,{Component} from 'react'
import logo from './logo.svg'
import './App.css'
import MyComponent from "./components/MyComponent"
import {createStore} from '@spyna/react-store'

class App extends Component {

  constructor(props){
    super(props)
    console.log(props)
  }

  // componentWillMount(){
  //   console.log('componentWillMount')
  // }

  // componentDidMount(){
  //    console.log('compoentDidMount')
  // }

  // componentWillReceiveProps(){
  //   console.log('componentWillReceiveProps')
  // }

  // shouldComponentUpdate(nextProps){
  //   console.log(nextProps, 'shouldComponentUpdate')
  //   return true
  // }

  // componentWillUpdate(){
  //   console.log("componentWillUpdate")
  // }

  // componentDidUpdate(){
  //   console.log('componentDidUpdate')
  // }

  // componentWillUnmount(){
  //   console.log('componentWillUnmount')
  // }

  render() {
    return (
      <div className="App" >
        <MyComponent />
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
        </a>
      </header>
    </div>
    )
  }
}

const initialValue = {
  amount: 15,
  username : {
    name : 'spyna',
    url : 'https://spyna.it'
  }
}


export default createStore(App, initialValue)