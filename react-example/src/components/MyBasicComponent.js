import React, { Component } from 'react'
import PropTypes from 'prop-types'



export default class MyBasicComponent extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    birth: PropTypes.number.isRequired,
    lang: PropTypes.string,
  }

  static defaultProps = {
    lang: 'Javascript',
  }

  static contextTypes = {
    router: PropTypes.object.isRequired,
  }

  state = {
    hidden: false,
  }

  constructor(props){
    // 메서드를 바인딩하거나 state를 초기화하는 작업이 없다면, 해당 React 컴포넌트에는 생성자를 구현하지 않아도 됩니다.
    
    // this.state에 객체를 할당하여 지역 state를 초기화
    // 인스턴스에 이벤트 처리 메서드를 바인딩
    // constructor() 내부에서 setState()를 호출하면 안 됩니다. 컴포넌트에 지역 state가 
    // 필요하다면 생성자 내에서 this.state에 초기 state 값을 할당하면 됩니다
    // 생성자는 this.state를 직접 할당할 수 있는 유일한 곳입니다
    console.log(constructor)
  }
  static getDerivedStateFromProps(props, state) {
    console.log('getDerivedStateFromProps')
  }
  
  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('getSnapshotBeforeUpdate')
  }
  

  componentWillMount() {
    console.log('componentWillMount - 앞으로 사용되지 않음')
  }

  componentDidMount() {
    console.log('componentDidMount')
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps - 앞으로 사용되지 않음')
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('shouldComponentUpdate')
    return true / false
  }

  componentWillUpdate(nextProps, nextState) {
    console.log('componentWillUpdate - 앞으로 사용되지 않음')
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate')
  }

  componentWillUnmount() {
    console.log('componentWillUnmount')
  }

  onClickButton = () => {
    this.setState({ hidden: true })
    this.refs.hide.disabled = true
  }

  componentDidCatch(error, info) {
    console.error(error, info)
  }

  render() {
    return (
      <div>
        <span>저는 {this.props.lang} 전문 {this.props.name}입니다!</span>
        {!this.state.hidden && <span>{this.props.birth}년에 태어났습니다.</span>}
        <button onClick={this.onClickButton} ref="hide">숨기기</button>
        <button onClick={this.context.router.goBack}>뒤로</button>
      </div>
    )
  }
}


// 1. mount

//   - constructor : state, context, defaultProps 저장 
//   - componentWillMount -> 이것은 앞으로 사용되지 않을 메소드이다.
//   - render
//   - componentDidMount

//   - constructor : state, context, defaultProps 저장
//   - getDerivedStateFromProps()
//   - render
//   - componentDidMount



// 2. Props Update

//   - componentWillReceiveProps : 이제는 사용하면 안됨
//   - shouldComponentUpdate  -> return false를 하면 component update를 취소 할 수 있음. 주로 여기서 성능 최적화
//   - componentWillUpdate : 이제는 사용하면 안됨 -> 이 함수에서는 state를 바꾸면 안됨. (무한 루프에 빠질 수 있음.)
//   - render
//   - componentDidUpdate --> render가 완료되었기 때문에 DOM에 접근 가능

//   - getDerivedStateFromProps()
//   - shouldComponentUpdate  -> return false를 하면 component update를 취소 할 수 있음. 주로 여기서 성능 최적화
//   - render
//   - getSnapshotBeforeUpdate()
//   - componentDidUpdate --> render가 완료되었기 때문에 DOM에 접근 가능


// 3. State Update -> setState를 통해 state가 업데이트 될때의 과정

//   - shouldComponentUpdate
//   - componentWillUpdate : 이제는 사용하면 안되는 녀석들
//   - render
//   - componentDidUpdate -> 두번째 인자로 바뀌기 이전의 state에 대한 정보를 가지고 있음

//   - shouldComponentUpdate
//   - render
//   - getSnapshotBeforeUpdate()
//   - componentDidUpdate -> 두번째 인자로 바뀌기 이전의 state에 대한 정보를 가지고 있음


// 4. unmount : 컴포넌트가 제거되는것..
//   - componentWillUnmount

// 5. 에러 상황에서
//  - getDerivedStateFromError()
//  - componentDidCatch()

// API
//   - setState
//   - force update

// class property
//   - defaultProps
//   - displayName

// instace property
//   - props
//   - state