// MyComponent.js
import React, { Component } from 'react'
import { withStore } from '@spyna/react-store'

class MyComponent extends Component {
    componentDidMount() {
        this.props.store.set('name', 'Spyna')
    }
    render() {
        return (
            <div>
                Hello {this.props.store.get('username').name}<br />
                {/* Hello {this.props.store.username.name}<br /> -> error 발생 */}
                Hello {this.props.store.get('test_name', { 'name': 'test' }).name}<br /> {/* defalut value test */}
                <p>My Amount: {this.props.store.get('amount')}</p>
            </div>
        )

    }
}
export default withStore(MyComponent)