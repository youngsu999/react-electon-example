# Electron + Clojure test 및 학습을 위한 repository임

여기에 작업하는 작업물들은 향후 다음 standard를 따른다

- ES6
- React JSX (class component -> functional component로 이전 진행)
- javascript standards


## init-example

basic electron example


```js
const { app, BrowserWindow } = require('electron')
function createWindow() {
    // Create the browser window.
    const win = new BrowserWindow({ width: 800, height: 600 })
    win.webContents.openDevTools()

    // and load the index.html of the app.
    win.loadFile('index.html')
}
app.on('ready', createWindow)
```

`package.json`

```json
{
  "name": "react-electron-example",
  "version": "1.0.0",
  "description": "- 다음의 링크를 기초로 작업합   - https://medium.com/@brockhoff/using-electron-with-react-the-basics-e93f9761f86f",
  "main": "Main.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "electron .",
    "lint": "eslint src"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "electron": "^6.0.10",
    "eslint": "^6.5.1",
    "eslint-plugin-react": "^7.16.0"
  }
}
```

### 1. 새로운 react app 만들기

```
# 모듈을 로컬 환경에 설치 하지 않고 실행할 수있도록 하는 명령어 `npx`
npx create-react-app react-example
# npx를 사용할 수 없는 경우 global module로 설치하여 실행.
npm install -g create-react-app
create-react-app react-example
```

### 2. Install electron


```
cd react-example
npm install --save-dev electron
```

### 3. introduce electron

react app을 electron으로 띄울 수 있도록 만들어주는 스크립트인 `main.js` 추가하기

```js
const { app, BrowserWindow } = require('electron')
function createWindow() {
    // Create the browser window.     
    win = new BrowserWindow({ width: 800, height: 600 })
    win.webContents.openDevTools()

    // and load the index.html of the app.     
    win.loadURL('http://localhost:3000/')
}
app.on('ready', createWindow)

```

참조 - https://www.freecodecamp.org/news/building-an-electron-application-with-create-react-app-97945861647c/

`package.json` 파일 중에 다음과 같은 항목을 추가함. (windowws에서는 생각보다 잘 동작하지 않음.))


```json
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "electron-start": "electron .",
    // "electron-dev": "ELECTRON_START_URL=http://localhost:3000 electron .", //-> os x, linux
    "electron-dev": "set ELECTRON_START_URL=http://localhost:3000 && electron .", // -> windows
  },
```


다음을 두개의 터미널에서 순차적으로 실행해야 함.

```bash
npm run start
```

```bash
npm run electron-start
```



## react-example

basic electron + react example





## 4. foreman을 통해서 프로세스를 관리하도록 작업

Procfile

```
react: npm start
electron: node src/electron-wait-react
```

```json
{
  "name": "react-example",
  "version": "0.1.0",
  "private": true,
  "main": "public/electron-starter.js",
  "dependencies": {
    "@spyna/react-store": "^1.4.0",
    "electron": "^6.0.10",
    "react": "^16.10.0",
    "react-dom": "^16.10.0",
    "react-scripts": "3.1.2"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject",
    "electron": "electron .",
    "dev": "nf start"
  },
  "eslintConfig": {
    "extends": "react-app"
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  },
  "devDependencies": {
    "electron": "^6.0.10",
    "electron-builder": "^21.2.0",
    "foreman": "^3.0.1"
  }
}
```
`main.js` 대신 다음 파일2개를 추가

- `src/electron-wait-react.js` : react app이 생성된 이후 electron이 실행되도록 기다려줌
- `public/electron-starter.js` : electron을 실행하도록 해주는...


 `src/electron-wait-react.js`

```js
const net = require('net')
const port = process.env.PORT ? (process.env.PORT - 100) : 3000

process.env.ELECTRON_START_URL = `http://localhost:${port}`

const client = new net.Socket()

let startedElectron = false
const tryConnection = () => client.connect({ port: port }, () => {
  client.end()
  if (!startedElectron) {
    console.log('starting electron')
    startedElectron = true
    const exec = require('child_process').exec
    exec('npm run electron')
  }
}
)

tryConnection()

client.on('error', (error) => {
  console.log(error);
  setTimeout(tryConnection, 1000)
})
```


`public/electron-starter.js`

```js
const { app, BrowserWindow } = require('electron')

const path = require('path')
const url = require('url')

const startUrl = process.env.ELECTRON_START_URL || url.format({
  pathname: path.join(__dirname, '../build/index.html'),
  protocol: 'file:',
  slashes: true
})
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({ width: 800, height: 600 })
  // win.webContents.openDevTools()
  mainWindow.loadURL(startUrl)
  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
```

### 5. redux 없이 state 관리하기 : test code를 만들어야 한다.

- react-stor moduel :[react-store](https://www.npmjs.com/package/@spyna/react-store) : class 기반 react-component에서 유용한 모듈임.

```bash
npm install --save @spyna/react-store 
```


### 참조

- [standardjs](https://standardjs.com/rules-kokr.html)
- [ES6 문법정리](https://jsdev.kr/t/es6/2944)
- [Using Electron with React: The Basics](https://medium.com/@brockhoff/using-electron-with-react-the-basics-e93f9761f86f)
  - basic 이라고는 하지만 실제로 많은 생략된 설명들이 있어서 따라하기 쉽지 않음.

- [React-create-react-app으로-프로젝트-시작하기](https://eunvanz.github.io/react/2018/06/05/React-create-react-app%EC%9C%BC%EB%A1%9C-%ED%94%84%EB%A1%9C%EC%A0%9D%ED%8A%B8-%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0/)
  - electron을 위해서 시작한 것은 아니고, 위의 문서에서 부족한 설명 부분을 참조하기 위함.
