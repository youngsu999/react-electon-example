import createEngine, { DiagramModel, DefaultNodeModel, DefaultLinkModel } from '@projectstorm/react-diagrams';
import * as React from 'react';

import { CanvasWidget } from '@projectstorm/react-canvas-core';
import { DemoCanvasWidget } from './DemoCanvasWidget';
import './App.css';

/**
 * Tests the grid size
 */
export default () => {
    //1) setup the diagram engine
    var engine = createEngine();

    //2) setup the diagram model
    var model = new DiagramModel();
    // model.setGridSize(50);

    //3-A) create a default node
    var node1 = new DefaultNodeModel('Node 1', 'rgb(0,192,255)');
    let port = node1.addOutPort('Out');
    node1.setPosition(100, 100);

    //3-B) create another default node
    var node2 = new DefaultNodeModel('Node 2', 'rgb(192,255,0)');
    let port2 = node2.addInPort('In');
    node2.setPosition(400, 100);

    // link the ports
    // let link1 = port.link(port2);
    let link1 = port.link<DefaultLinkModel>(port2);

    //4) add the models to the root graph
    model.addAll(node1, node2, link1);

    //5) load model into engine
    engine.setModel(model);

    const onClick = () => {
        //3-B) create another default node
        let node = new DefaultNodeModel('Node 2', 'rgb(192,255,0)');
        let port = node.addInPort('In');
        node.setPosition(300, 100);
        model.addAll(node)
        engine.setModel(model); //-> render 되고 canvas 의 정보를 읽어올 수 있음.
    }

    const onClick2 = () => {
        // let model_s = model.serialize()
        let str = JSON.stringify(model.serialize());
        let model_ = new DiagramModel();
        // model_.deserializeModel(model_s, engine)
        model_.deserializeModel(JSON.parse(str), engine);
        engine.setModel(model_)
        // garbage collection 이 issue가 될 수 있다.
        model = null
        model = model_
    }

    const onClick3 = () => {
        let model_s = model.serialize()
        // let str = JSON.stringify(model.serialize());
        let model_ = new DiagramModel();
        model_.deserializeModel(model_s, engine)
        // model_.deserializeModel(JSON.parse(str), engine);
        engine.setModel(model_)
        // garbage collection 이 issue가 될 수 있다.
        model = null
        model = model_
    }
    //6) render the diagram!
    return (
        <div>
            <DemoCanvasWidget >
                <CanvasWidget engine={engine} />
            </DemoCanvasWidget>
            <button onClick={onClick}>
                Activate Lasers
            </button>
            <button onClick={onClick2}>
                json string restore
            </button>
            <button onClick={onClick3}>
                json object restore
            </button>
        </div>
    );
};