import * as React from 'react';
import { render } from 'react-dom';

import Counter from './Counter';
import ReactDiagramWidget from './ReactDiagramWidget';

import './App.css';

// render(<Counter />, document.getElementById('main'));
render(<ReactDiagramWidget />, document.getElementById('root'));
